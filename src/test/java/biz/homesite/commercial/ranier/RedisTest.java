package biz.homesite.commercial.ranier;

import biz.homesite.commercial.ranier.async.redis.QuoteController;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

import java.time.Duration;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.SynchronousQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RedisTest {

    static final JedisPool jedisPool;

    static {
        final JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(128);
        poolConfig.setMaxIdle(128);
        poolConfig.setMinIdle(16);
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(true);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setMinEvictableIdleTimeMillis(Duration.ofSeconds(60).toMillis());
        poolConfig.setTimeBetweenEvictionRunsMillis(Duration.ofSeconds(30).toMillis());
        poolConfig.setNumTestsPerEvictionRun(3);
        poolConfig.setBlockWhenExhausted(true);

        jedisPool = new JedisPool(poolConfig, "localhost");


    }

    @Test
    public void connectivity() {
        Jedis jedis = jedisPool.getResource();
        String id = "4";
        jedis.set(id, "This is the result for id: " + 4);
        String result = jedis.get(id);
        assertTrue(result.isEmpty() == false);

    }

    @Test
    public void pubsub() {

        TaskExecutor theExecutor = new SimpleAsyncTaskExecutor();
String channelName = "/quote";

        theExecutor.execute(() -> {
            Jedis jedis = jedisPool.getResource();
            System.out.println("I am in the executor");
            Jedis jSubscriber = jedisPool.getResource();

            jSubscriber.subscribe(new JedisPubSub() {
                @Override
                public void onMessage(String channel, String message) {
                    System.out.println("The message is: " + message);
                    assertTrue(message != null && message.isEmpty() == false);
                }
            }, channelName);
            System.out.println("subscribed");
        });

        try {
            Thread.sleep(10000);

        Jedis jedisPub = jedisPool.getResource();
        jedisPub.publish(channelName, "Hey There!");
        System.out.println("published message");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
