package biz.homesite.commercial.ranier;


import biz.homesite.commercial.ranier.async.redis.QuoteController2;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class AsyncTest {

    @Test
    public void test() {



        ExecutorService executor = Executors.newFixedThreadPool(50);
        Callable<String> callableTask = () -> {
            QuoteController2 q2 = new QuoteController2();
            String id = q2.quote();
            System.out.println("The quote id is: " + id);
            //Thread.sleep(1000);
            System.out.println("polling: " + id);
            return q2.poll(id);
        };

        List<Callable<String>> callableTasks = new ArrayList<>();

        for (int i =0; i < 5; i++) {
            callableTasks.add(callableTask);
        }

        //Future<String> future = executor.submit(callableTask);
        try {
            List<Future<String>> futures = executor.invokeAll(callableTasks);
            futures.stream().parallel().forEach(future -> {
                try {
                    String result = future.get();
                    System.out.println("result is: " + result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


        executor.shutdown();

    }
}
