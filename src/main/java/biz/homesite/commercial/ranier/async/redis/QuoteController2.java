
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.homesite.commercial.ranier.async.redis;

import java.time.Duration;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

@RestController
public class QuoteController2 {

    static final JedisPool jedisPool;

    static final ConcurrentHashMap<String, SynchronousQueue> listeners = new ConcurrentHashMap<>();

    Random random = new Random();

    static {
        final JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(128);
        poolConfig.setMaxIdle(128);
        poolConfig.setMinIdle(16);
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(true);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setMinEvictableIdleTimeMillis(Duration.ofSeconds(60).toMillis());
        poolConfig.setTimeBetweenEvictionRunsMillis(Duration.ofSeconds(30).toMillis());
        poolConfig.setNumTestsPerEvictionRun(3);
        poolConfig.setBlockWhenExhausted(true);

        jedisPool = new JedisPool(poolConfig, "localhost");

        TaskExecutor theExecutor = new SimpleAsyncTaskExecutor();

        theExecutor.execute(() -> {
            Jedis jedis = jedisPool.getResource();

            Jedis jSubscriber = jedisPool.getResource();

            jSubscriber.subscribe(new JedisPubSub() {
                @Override
                public void onMessage(String channel, String message) {
                    SynchronousQueue listener = listeners.get(message);

                    if (listener != null) {
                        try {
                            listener.put(message);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(QuoteController.class.getName()).log(Level.SEVERE, null, ex);
                            ex.printStackTrace();
                        }
                    }
                }
            }, "/quote");
        });
    }

    @RequestMapping("/hello")
    public String hello() {
        return "hello";
    }

    @Async
    @RequestMapping("/quote")
    public String quote() {

        // generate id
        String id = UUID.randomUUID().toString();
        
        ExecutorService es = Executors.newSingleThreadExecutor();
        es.execute(() -> {
            Jedis jedis = null;
            try {
                //invoke backend (i.e. sleep for as specified amount of time

                Thread.sleep(random.nextInt(10000));
                //Thread.sleep(5000);
                //place value in db
                System.out.println("I have finished sleeping: " + id);
                jedis = jedisPool.getResource();
                jedis.set(id, "This is the result for id: " + id);
                //raise event with id
                jedis.publish("/quote", id);

            } catch (InterruptedException ex) {
                System.out.println("Error: " + ex.getMessage() + " id: " + id);
            } finally {
                jedis.close();
            }
        });

        return id;
    }

    @RequestMapping("/quote/poll/{id}")
    public String poll(@PathVariable("id") String id) {

        String response = null;

        try {

            Jedis jedisA = jedisPool.getResource();
            response = jedisA.get(id);
            jedisA.close();

            CompletableFuture<String> listener
                    = CompletableFuture.supplyAsync(() -> {

                        SynchronousQueue q = new SynchronousQueue();
                        listeners.put(id, q);
                        try {
                            q.take();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(QuoteController.class.getName()).log(Level.SEVERE, null, ex);
                        } finally {
                            listeners.remove(id);
                        }
                        String value;

                        
                        Jedis jedisB = jedisPool.getResource();
                        value = jedisB.get(id);
                        jedisB.close();

                        return value;
                    });
 
            if (response == null) {
                response = listener.get(1, TimeUnit.MINUTES);
            }
            
            

        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            //System.out.println("Error: " + e.getMessage() + " message id: " + id);
            //e.printStackTrace();
            listeners.remove(id);
            response = "408 Request timeout, id: " + id;
        }

        return response;
    }
}
